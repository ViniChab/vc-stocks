import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

// Pretty simple toggle component, didn't implement ControlValueAccessor or an output
// because it wouldn't really be used
@Component({
  selector: 'app-toggle-switch',
  templateUrl: './toggle-switch.component.html',
  styleUrls: ['./toggle-switch.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
})
export class ToggleSwitchComponent {
  @Input() public label!: string;
  @Input() public checked!: boolean;
}
