import { createFeatureSelector, createSelector } from '@ngrx/store';
import { StockState } from './stock.store.reducer';

export const stockState = createFeatureSelector<StockState>('stock');

export const getStocks = createSelector(stockState, (state) => state.stocks);

export const isWebsocketConnected = createSelector(
  stockState,
  (state) => state.websocketConnected
);
