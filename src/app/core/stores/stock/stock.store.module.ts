import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { YahooApiService, YahooWebsocketService } from '../../services';
import { StockEffects } from './stock.store.effect';
import { stockReducer } from './stock.store.reducer';

@NgModule({
  imports: [
    EffectsModule.forFeature([StockEffects]),
    StoreModule.forFeature('stock', stockReducer),
  ],
  providers: [YahooWebsocketService, YahooApiService],
})
export class StockStoreModule {}
