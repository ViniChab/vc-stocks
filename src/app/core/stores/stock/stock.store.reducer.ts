import { createReducer, on } from '@ngrx/store';
import { Stock } from '../../interfaces';
import {
  startYahooWebsocketSuccess,
  startYahooWebsocketError,
  updateStocksSuccess,
  getStockDataSuccess,
  getStockDataError,
  enableStock,
  disableStock,
} from './stock.store.action';

export interface StockState {
  stocks: Stock[];
  action: StockStoreAction;
  message: string;
  websocketConnected: boolean;
}

export enum StockStoreAction {
  noAction,
  startingYahooWebsocketSuccess,
  startingYahooWebsocketError,
  getStockDataSuccessSuccess,
  getStockDataSuccessError,
  updatedWebsocketDataSuccess,
}

const initialState: StockState = {
  stocks: [
    { id: 'BTC-USD', enabled: true },
    { id: 'ETH-USD', enabled: true },
    { id: 'XMR-USD', enabled: true },
    { id: 'BNB-USD', enabled: true },
  ],
  action: StockStoreAction.noAction,
  message: '',
  websocketConnected: false,
};

export const stockReducer = createReducer(
  initialState,
  on(startYahooWebsocketSuccess, (state) => {
    state = {
      ...state,
      action: StockStoreAction.startingYahooWebsocketSuccess,
      websocketConnected: true,
    };
    return state;
  }),
  on(startYahooWebsocketError, (state, res) => {
    state = {
      ...state,
      action: StockStoreAction.startingYahooWebsocketError,
      message: res.error,
    };
    return state;
  }),
  on(updateStocksSuccess, (state, res) => {
    if (res.stock.id) {
      let newStock = { ...res.stock };
      const stocks = [...state.stocks];
      const index = state.stocks.findIndex(
        (stock) => stock.id === res.stock.id
      );

      if (stocks[index].enabled) {
        newStock = {
          ...newStock,
          historicalData: stocks[index].historicalData,
        };
        stocks[index] = { ...stocks[index], ...newStock };
      }

      state = {
        ...state,
        action: StockStoreAction.updatedWebsocketDataSuccess,
        stocks,
      };
    }
    return state;
  }),
  on(getStockDataSuccess, (state, res) => {
    const { data } = res;
    const stocks = [...state.stocks];
    const index = state.stocks.findIndex((s) => s.id === data.stockId);
    stocks[index] = {
      ...stocks[index],
      historicalData: data,
      price: stocks[index].price ?? data.open,
    };

    state = {
      ...state,
      stocks,
      action: StockStoreAction.getStockDataSuccessSuccess,
    };
    return state;
  }),
  on(getStockDataError, (state, res) => {
    state = {
      ...state,
      action: StockStoreAction.getStockDataSuccessError,
      message: res.error,
    };

    return state;
  }),
  on(enableStock, (state, action) => {
    const stocks = [...state.stocks];
    const index = stocks.findIndex((s) => s.id === action.stock.id);
    stocks[index] = { ...stocks[index], enabled: true };

    state = { ...state, stocks };
    return state;
  }),
  on(disableStock, (state, action) => {
    const stocks = [...state.stocks];
    const index = stocks.findIndex((s) => s.id === action.stock.id);
    stocks[index] = { ...stocks[index], enabled: false };

    state = { ...state, stocks };
    return state;
  })
);
