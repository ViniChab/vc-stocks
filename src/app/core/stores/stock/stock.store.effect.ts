import { inject, Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, exhaustMap, map, mergeMap } from 'rxjs/operators';
import { StockState } from '.';
import { Stock, StockHistoricalData } from '../../interfaces';
import { YahooApiService, YahooWebsocketService } from '../../services';
import {
  startYahooWebsocket,
  startYahooWebsocketSuccess,
  startYahooWebsocketError,
  updateStocks,
  updateStocksSuccess,
  getStockData,
  getStockDataSuccess,
  getStockDataError,
} from './stock.store.action';

@Injectable()
export class StockEffects {
  private stockStore = inject(Store<StockState>);
  private actions$: Actions<Action> = inject(Actions);
  private websocketService = inject(YahooWebsocketService);
  private apiService = inject(YahooApiService);

  startWebsocket$ = createEffect(() =>
    this.actions$.pipe(
      ofType(startYahooWebsocket),
      exhaustMap(() =>
        this.websocketService.startWebsocket().pipe(
          map(() => {
            this.stockStore.dispatch(updateStocks());
            return startYahooWebsocketSuccess();
          }),
          catchError((error) => of(startYahooWebsocketError({ error })))
        )
      )
    )
  );

  updateStocks$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateStocks),
      exhaustMap(() =>
        this.websocketService
          .getWebsocketUpdates()
          .pipe(
            map((stock) => updateStocksSuccess({ stock: new Stock(stock) }))
          )
      )
    )
  );

  getStockData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getStockData),
      mergeMap((action) =>
        this.apiService.getStockData(action.stockName).pipe(
          map((stock) =>
            getStockDataSuccess({
              data: new StockHistoricalData({
                ...stock.summaryDetail,
                stockId: action.stockName,
              }),
            })
          ),
          catchError((error) => of(getStockDataError({ error })))
        )
      )
    )
  );
}
