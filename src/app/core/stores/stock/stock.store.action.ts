import { createAction, props } from '@ngrx/store';
import { Stock, StockHistoricalData } from '../../interfaces';

export const startYahooWebsocket = createAction(
  '[Stock] Start Yahoo Websocket'
);

export const startYahooWebsocketSuccess = createAction(
  '[Stock] Start Yahoo Websocket Success'
);

export const startYahooWebsocketError = createAction(
  '[Stock] Start Yahoo Websocket Error',
  props<{ error: string }>()
);

export const getStockData = createAction(
  '[Stock] Get Stock Data',
  props<{ stockName: string }>()
);

export const getStockDataSuccess = createAction(
  '[Stock] Get Stock Data Success',
  props<{ data: StockHistoricalData }>()
);

export const getStockDataError = createAction(
  '[Stock] Get Stock Data Error',
  props<{ error: string }>()
);

export const updateStocks = createAction('[Stock] Update Stock Data');

export const updateStocksSuccess = createAction(
  '[Stock] Update Stock Data Success',
  props<{ stock: Stock }>()
);

export const enableStock = createAction(
  '[Stock] Enable Stock',
  props<{ stock: Stock }>()
);

export const disableStock = createAction(
  '[Stock] Disable Stock',
  props<{ stock: Stock }>()
);
