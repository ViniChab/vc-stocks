import { HttpClient, HttpHeaders } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { environment } from 'src/environment/environment';
import { RawStockHistoricalData } from '../../interfaces';

@Injectable()
export class YahooApiService {
  private httpClinet = inject(HttpClient);

  public getStockData(stockName: string) {
    return this.httpClinet.get<{ summaryDetail: RawStockHistoricalData }>(
      `${environment.API_URL}&symbol=${stockName}`,
      {
        headers: new HttpHeaders({ 'x-rapidapi-key': environment.API_KEY }),
      }
    );
  }
}
