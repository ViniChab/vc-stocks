import { Injectable } from '@angular/core';
import * as protobuf from 'protobufjs';
import { Buffer } from 'buffer';
import { Observable } from 'rxjs';
import { Stock } from '../../interfaces';
import { environment } from 'src/environment/environment';

@Injectable()
export class YahooWebsocketService {
  private decoder!: protobuf.Type;
  private wsInstance = new WebSocket(environment.WS_URL);

  public startWebsocket(): Observable<string> {
    return new Observable((observer) => {
      protobuf.load(environment.PROTO_FILE, (error, root) => {
        if (error || !root) {
          observer.error(`Error loading protobuf: ${error ?? 'No root'}`);
          return;
        }

        this.wsInstance.onopen = () => {
          observer.next('Websocket started');
          this.decoder = root.lookupType('yahooResponse');
          this.wsInstance.send(
            JSON.stringify({ subscribe: environment.STOCKS })
          );
        };
      });
    });
  }

  public getWebsocketUpdates(): Observable<Stock> {
    return new Observable((observer) => {
      this.wsInstance.onmessage = (res) => {
        const encodedData = res.data as string;
        const decodedData = this.decoder.decode(
          Buffer.from(encodedData, 'base64')
        ) as unknown;

        observer.next(decodedData as Stock);
      };
    });
  }
}
