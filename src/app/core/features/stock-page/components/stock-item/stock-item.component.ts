import { CommonModule, CurrencyPipe } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  HostListener,
  Input,
  Output,
} from '@angular/core';
import { TranslocoModule } from '@ngneat/transloco';
import { ToggleSwitchComponent } from '../../../../components/toggle-switch/toggle-switch.component';
import { Stock } from '../../../../interfaces';

@Component({
  selector: 'app-stock-item',
  templateUrl: './stock-item.component.html',
  styleUrls: ['./stock-item.component.scss'],
  imports: [CommonModule, CurrencyPipe, TranslocoModule, ToggleSwitchComponent],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
})
export class StockItemComponent {
  @Input() public stock!: Stock;
  @Output() public disableStock = new EventEmitter<Stock>();
  @Output() public enableStock = new EventEmitter<Stock>();

  @HostListener('click')
  public onClick(): void {
    this.stock.enabled
      ? this.disableStock.emit(this.stock)
      : this.enableStock.emit(this.stock);
  }
}
