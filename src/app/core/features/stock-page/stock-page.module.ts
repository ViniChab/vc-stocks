import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StockPageComponent } from './stock-page.component';
import { RouterModule, Routes } from '@angular/router';
import { StockStoreModule } from '../../stores/stock';
import { StockItemComponent } from './components/stock-item/stock-item.component';
import { PageLoadingComponent } from '../../components/page-loading/page-loading.component';
import { TranslocoModule } from '@ngneat/transloco';

const routes: Routes = [
  {
    path: '',
    component: StockPageComponent,
  },
];

@NgModule({
  declarations: [StockPageComponent],
  imports: [
    CommonModule,
    StockStoreModule,
    TranslocoModule,
    RouterModule.forChild(routes),
    StockItemComponent,
    PageLoadingComponent,
  ],
})
export class StockPageModule {}
