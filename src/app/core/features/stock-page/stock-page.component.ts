import {
  ChangeDetectionStrategy,
  Component,
  inject,
  OnInit,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { environment } from 'src/environment/environment';
import { Stock } from '../../interfaces';
import {
  disableStock,
  enableStock,
  getStockData,
  getStocks,
  isWebsocketConnected,
  startYahooWebsocket,
  StockState,
} from '../../stores/stock';

@Component({
  templateUrl: './stock-page.component.html',
  styleUrls: ['./stock-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StockPageComponent implements OnInit {
  private stockStore = inject(Store<StockState>);
  public stocks$ = this.stockStore.select(getStocks);
  public hasStarted$ = this.stockStore.select(isWebsocketConnected);
  public stocks: Stock[] = [];

  ngOnInit(): void {
    this.stockStore.dispatch(startYahooWebsocket());

    environment.STOCKS.forEach((stockName) =>
      this.stockStore.dispatch(getStockData({ stockName }))
    );
  }

  public trackStocks(index: number, stock: Stock): string {
    return stock.id;
  }

  public onDisableStock(stock: Stock): void {
    this.stockStore.dispatch(disableStock({ stock }));
  }

  public onEnableStock(stock: Stock): void {
    this.stockStore.dispatch(enableStock({ stock }));
  }
}
