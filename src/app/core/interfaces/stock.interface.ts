// Decided to create classes here (instead of interfaces) in order to remove most of the junk that comes from the API.

export class Stock {
  id!: string;
  enabled!: boolean;
  changePercent?: number;
  change?: number;
  price?: number;
  historicalData?: StockHistoricalData;

  constructor(stock: Stock) {
    this.id = stock.id;
    this.enabled = stock.enabled ?? true;
    this.changePercent = stock.changePercent;
    this.change = stock.change;
    this.price = stock.price;
    this.historicalData = stock.historicalData;
  }
}

export class StockHistoricalData {
  stockId!: string;
  fiftyTwoWeekHigh!: number;
  fiftyTwoWeekLow!: number;
  regularMarketDayHigh!: number;
  regularMarketDayLow!: number;
  open!: number;

  constructor(stockHistoricalData: RawStockHistoricalData) {
    this.stockId = stockHistoricalData.stockId;
    this.fiftyTwoWeekHigh = stockHistoricalData.fiftyTwoWeekHigh.raw;
    this.fiftyTwoWeekLow = stockHistoricalData.fiftyTwoWeekLow.raw;
    this.regularMarketDayHigh = stockHistoricalData.regularMarketDayHigh.raw;
    this.regularMarketDayLow = stockHistoricalData.regularMarketDayLow.raw;
    this.open = stockHistoricalData.open.raw;
  }
}

export interface RawStockHistoricalData {
  stockId: string;
  fiftyTwoWeekHigh: { raw: number };
  fiftyTwoWeekLow: { raw: number };
  regularMarketDayHigh: { raw: number };
  regularMarketDayLow: { raw: number };
  open: { raw: number };
}
