export const environment = {
  STOCKS: ['BTC-USD', 'ETH-USD', 'XMR-USD', 'BNB-USD'],
  WS_URL: 'wss://streamer.finance.yahoo.com',
  PROTO_FILE: 'protobuf.proto',
  API_URL: 'https://yh-finance.p.rapidapi.com/stock/v2/get-summary?region=US',
  API_KEY: '604cf9e29bmsh6070b6565d2c3e1p1d32c3jsnbfb0c7194445', // Just leaving this here for simplicity
};
