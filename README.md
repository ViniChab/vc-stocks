# VC Stocks

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.0.5.

# Running the application

First, clone this repository via HTTPS;

Then, go the project root folder and run `npm i`, this will install all required dependencies;

Lastly, run `npm start`. The project will start on `localhost:4200`:

![Stock app image](https://i.imgur.com/kMHp029.png)
